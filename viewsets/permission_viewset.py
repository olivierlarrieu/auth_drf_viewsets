from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import filters
from django.contrib.auth.models import Permission


class PermissionSerializer(serializers.ModelSerializer):
    '''
    Permission serializer
    '''

    class Meta:
        model = Permission
        fields = ['id', 'name', 'codename']


class PermissionViewset(viewsets.ModelViewSet):
    '''
    Permission ModelViewSet
    '''

    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filter_fields = ['id', 'name', 'codename']
    search_fields = ['id', 'name', 'codename']