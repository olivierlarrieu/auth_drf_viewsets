from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import filters
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    '''
    User serializer
    '''

    class Meta:
        model = User
        fields = ['id', 'password', 'last_login', 'is_superuser', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined']


class UserViewset(viewsets.ModelViewSet):
    '''
    User ModelViewSet
    '''

    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter)
    filter_fields = ['id', 'password', 'last_login', 'is_superuser', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined']
    search_fields = ['id', 'password', 'last_login', 'is_superuser', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined']