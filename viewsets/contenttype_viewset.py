from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import filters
from django.contrib.auth.models import ContentType

class ContentTypeSerializer(serializers.ModelSerializer):
    '''
    ContentType serializer
    '''

    class Meta:
        model = ContentType
        fields = ['id', 'app_label', 'model']


class ContentTypeViewset(viewsets.ModelViewSet):
    '''
    ContentType ModelViewSet
    '''

    queryset = ContentType.objects.all()
    serializer_class = ContentTypeSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filter_fields = ['id', 'app_label', 'model']
    search_fields = ['id', 'app_label', 'model']