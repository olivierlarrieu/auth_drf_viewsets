from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import filters
from django.contrib.auth.models import Group


class GroupSerializer(serializers.ModelSerializer):
    '''
    Group serializer
    '''

    class Meta:
        model = Group
        fields = ['id', 'name']


class GroupViewset(viewsets.ModelViewSet):
    '''
    Group ModelViewSet
    '''

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filter_fields = ['id', 'name']
    search_fields = ['id', 'name']