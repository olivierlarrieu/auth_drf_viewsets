from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from auth_manage.viewsets.contenttype_viewset import ContentTypeViewset
from auth_manage.viewsets.group_viewset import GroupViewset
from auth_manage.viewsets.permission_viewset import PermissionViewset

from auth_manage.viewsets.user_viewset import UserViewset

router = DefaultRouter()
router.register(r"contenttype", ContentTypeViewset)
router.register(r"group", GroupViewset)
router.register(r"permission", PermissionViewset)
router.register(r"user", UserViewset)

urlpatterns = [
    url(r"^", include(router.urls)),
]